<?php

if( empty( $_POST ) ) {  // проверяем пришел ли ответ
    echo '<p style="text-align:center;">Something was wrong($_POST is empty)</p>';
    die;
}

$private_key = 'sandbox_66n9MFWE0zQ1f1zh2TJEum4KQi0MqbPWp2lIaAKp';  // (тестовый)приватный ключ liqpay
$signature   =  base64_encode( sha1( $private_key . $_POST['data'] . $private_key, 1 ) );  // формируем подпись на сервере для сравнения с подписью из ответа
if( ! $signature == $_POST['signature'] ) {  // проверяем подписи на идентичность
    echo '<p style="text-align:center;">Data was corrupted</p>';
    die;
}
$request = json_decode( base64_decode( $_POST['data'] ), true );  // декодируем строку данных из ответа в массив

require_once __DIR__ . '/db/db_ini_rb.php';  // подключаем для работы с БД
$order = R::load( 'orders', (int)$request['order_id'] );  // загружаем строку БД в объект $order
if( ! $order ) {  // проверяем существует ли такая запись в БД
    echo '<p style="text-align:center;">Something was wrong(Order not exists)</p>';
    die;
}

if( $request['status'] != 'success' || $request['amount'] != $order->price ) {  // проверка параметров ответа
    echo '<p style="text-align:center;">Payment was not successful or incorrect</p>';
    die;
}

// обновление и сохранение в БД
$order->status = '1';
R::store( $order );
echo '<p style="text-align:center;">Thank you, ' . $order->name . '! Payment was successful</p>';
