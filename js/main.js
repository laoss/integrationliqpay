$( '.buy' ).click( function() {
    var price = $( this ).data( 'price' ),
        product = $( this ).data( 'product' );
    $( '#price' ).val( price + " UAH" );
    $( '#product' ).val( product );
    $( '#popup-form' ).modal();

    console.log(price, product);
    return false;
});