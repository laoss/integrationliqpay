<?php  // working moments
unset( $db_type );
@include "db_ini.php";
if( empty( $db_type ) ) {
    echo "Config load error";
    exit;
}
$conStr = "$db_type:host=$db_host;dbname=$db_name;charset=$db_enc;";
try{
    $DB = new PDO( $conStr, $db_user, $db_pass );
    $DB->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
} catch( PDOException $ex ) {
    echo "CONNECTION ERROR: ", $ex->getMessage();
    exit;
}

$query = <<<SQL
CREATE TABLE IF NOT EXISTS orders(
id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(64),
email VARCHAR(64),
product VARCHAR(64),
price FLOAT,
ordered TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
status BOOLEAN DEFAULT NULL
) engine = InnoDB default charset = utf8 collate = utf8_general_ci
SQL;

try{ 
    $DB->query($query);
} catch( Exception $ex ) {
    echo $ex->getMessage(), "<BR>", $query;
    exit;
}
echo "<br/>Query OK";