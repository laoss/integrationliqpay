<?php

unset( $db_type );  // удаление $db_type
@include "db_ini.php";  // подключение данных для соединения с сервером БД
if( empty( $db_type ) ) {  // проверка на пустоту $db_type
    echo "Config load error";    
    exit;
}

$conStr = "$db_type:host=$db_host;dbname=$db_name;charset=$db_enc;";  // формирование строки подключения

require_once 'rb-mysql.php';  // подключение orm redbean
R::setup( $conStr, $db_user, $db_pass );  // установка соединения через rbphp
R::freeze( true );  // отключене измений на лету

