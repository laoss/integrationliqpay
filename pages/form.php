<?php

session_start();

if( ! empty( $_SESSION['payment'] ) ) {
    $private_key = 'sandbox_66n9MFWE0zQ1f1zh2TJEum4KQi0MqbPWp2lIaAKp';  // (тестовый)приватный ключ liqpay
    $json_array = array(  // заполнение массива данными для формирования json строки
        'public_key'  => 'sandbox_i30546079831',
        'version'     => '3',
        'action'      => 'pay',
        'amount'      => sprintf((int)$_SESSION['payment']['price']),  // паттерн бвк
        'currency'    => 'UAH',
        'description' => 'Оплата товара: ' . $_SESSION['payment']['product'],
        'order_id'   =>  (string)$_SESSION['payment']['id'],        
        'server_url'  => 'http://www.practsite.space/wscripts/process.php',  // куда придет ответ после платежа
        'result_url'  => 'http://www.practsite.space/pages/result.php'  // страница перенаправления после платежа
    );
    $json_string = json_encode( $json_array, JSON_UNESCAPED_UNICODE );  // JSON_UNESCAPED_UNICODE для сохранения кириллицы

    $data        = base64_encode( $json_string );  // готовим data для передачи в форму
    $sign_string = $private_key . $data . $private_key;  
    $signature   = base64_encode( sha1( $sign_string, true ) );  // готовим signature для передачи в форму
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Переадресация</title>
</head>
<body>
<p style="text-align:center;">Через несколько секунд Вы будете перенаправлены на страницу оплаты.<br> Нажмите кнопку если не хотите ждать...</p>

<form method="POST" action="https://www.liqpay.ua/api/3/checkout" accept-charset="utf-8">
    <input type="hidden" name="data" value="<?=$data;?>"/>
    <input type="hidden" name="signature" value="<?=$signature;?>"/>
    <div style="text-align: center;">
        <input type="image" src="//static.liqpay.ua/buttons/p1ru.radius.png"/>
    </div>
</form>  
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" 
        crossorigin="anonymous">
</script>
<script>
    window.setTimeout( function() {  // запланированная отправка формы
        $( 'form' ).submit();
    }, 4000 );
</script>
</body>
</html>