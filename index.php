<?php //echo $_SERVER['PHP_SELF'] . "<br>" . $_SERVER['DOCUMENT_ROOT'];
require_once '/home/practsit/public_html/wscripts/functions.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
          crossorigin="anonymous" >
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" href="puc/favicon.png">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/index.php">Laoss & co.</a>
  <button class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/pages/about_us.html">About us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/pages/contacts.html">Contacts</a>        
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 1</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">100 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="100" data-product="Product 1">Buy</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 2</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">60 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="60" data-product="Product 2">Buy</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 3</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">79 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="79" data-product="Product 3">Buy</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 4</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">5 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="5" data-product="Product 4">Buy</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 5</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">500 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="500" data-product="Product 5">Buy</a>
                    </div>
                </div>
            </div>
        
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <img src="puc/product.jpg" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Product 6</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="price text-danger">106 UAH</p>
                        <a href="#" class="btn btn-primary buy" data-price="106" data-product="Product 6">Buy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="popup-form" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="purchase-confirm" method="post" >
            <div class="form-group">
                <label for="name">Your name</label>
                <input type="name" name="name" class="form-control" id="name" aria-describedby="emailHelp" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" required>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>            
            </div>
            <div class="form-group">                
                <label for="product">Selected item</label>
                <input type="text" name="product" class="form-control" id="product" readonly>
            </div>
            <div class="form-group">                
                <label for="price">Price</label>
                <input type="text" name="price" class="form-control" id="price" readonly>
            </div>
            <button type="submit" class="btn btn-primary">Valide purchase</button>
        </form>
      </div>      
    </div>
  </div>
</div>

<footer class="blog-footer">
  <p>&copy; 2001-2020 Laoss & co. all rights reserved</p>
  <p>
    <a href="#">Back to top</a>
  </p>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" 
        crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous">
</script>
<script src="js/main.js"></script>
</body>
</html>