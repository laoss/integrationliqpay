<?php

session_start();

$data = [
    'name'    => '',
    'email'   => '',
    'product' => '',
    'price'   => ''
];

if( ! empty( $_POST ) ) {
    require_once '/home/practsit/public_html/db/db_ini_rb.php';  // подключение orm redbeanphp
    $data = load( $data );  // заполнение массива $data данными из $_POST
    $order_id = save( 'orders', $data );  // сохранение в БД 
    setPaymentData( $order_id );  // сохранение данных в сессию для передачи в форму оплаты
    header( 'Location: pages/form.php' );  // перенаправление на страницу с формой для оплаты
    die;  // завершениe работы скрипта
}

function setPaymentData( $order_id ) {  // ф-я записывает в $_SESSION данные для оплаты
    if( isset( $_SESSION['payment'] ) )
        unset( $_SESSION['payment'] );        
    $_SESSION['payment']['id']      = $order_id;
    $_SESSION['payment']['price']   = $_POST['price'];
    $_SESSION['payment']['product'] = $_POST['product'];
}

function load( $data ) {  //ф-я заполняет массив данными из $_POST
    foreach( $_POST as $k => $v ) {
        if( array_key_exists( $k, $data ) ) {
            $data[$k] = $v;            
        }
    }
    return $data;
}

function save( $table, $data ) {  // ф-я сохраняет данные в БД, возвращает id строки в БД
    $tbl = R::dispense( $table );
    foreach( $data as $k => $v )
        $tbl[$k] = $v;
    return R::store( $tbl );
}

function debug( $data ) {  // ф-я для отладки
    echo '<pre>' . print_r( $data, true ) . '</pre>';
}